#include <stdio.h>
#include <stdlib.h>


#define INFINITY -1


/*estruturas*/
typedef struct listNode *link;
typedef struct vertex vertice;

/*lista simplesmente ligada*/
struct listNode {
	struct vertex* v;
	struct listNode *next;
	struct listNode *nextResidual;
	/* entre o proprio e o proximo*/
	int peso;
};


struct queue {
	int* array;
	int begin;
	int end;
};

/*vertice de um grafo*/
struct vertex{
	int id;
	int d;
	struct vertex* predecessor;
	struct listNode* path;
	int minCost;
	/*lista dos vertices adjacentes ao vertice*/
	struct listNode* arcos_lista;
	int pesoBranco;
	int pesoPreto;
	char color;
};


/*grafo (representado por uma lista de adjacencias)*/
typedef struct graph {
	struct vertex* vertices;
	int numberOfVertexes;
	int flow;
	struct vertex* source;
	struct vertex* sink;
	struct queue* graphq;

}graph;



/*adiciona um elemento a uma lista na primeira posicao*/
link add2list(link head, struct vertex *e, int peso) {
	link newNode = (link)malloc(sizeof (struct listNode));
	newNode->v = e;
	newNode->next = head;
	newNode->peso = peso;
	newNode->nextResidual = NULL;
	return newNode;
}

/*faz push para uma list que e um stack*/
struct queue* enqueue (struct queue *queue, struct vertex* v) {
	queue->array[queue->end++] = v->id;
	return queue;
}



/*faz pop  de um stack (retira o primeiro elemento da lista)*/
vertice* dequeue (graph* g, struct queue *queue) {
	if (queue->end == queue->begin) return NULL;
	return &g->vertices[queue->array[queue->begin++]];
}

/*construtor do vertice*/
vertice createVertex(int id) {
	vertice newVertice;
	newVertice.id = id;
	newVertice.d = -10;
	newVertice.arcos_lista = NULL;
	newVertice.predecessor = NULL;
	newVertice.pesoBranco = INFINITY;
	newVertice.pesoPreto = INFINITY;
	newVertice.minCost = INFINITY;
	newVertice.path = NULL;
	newVertice.color = 'A';
	return newVertice;
}

/*construtor do grafo*/
graph* initGraph (int size) {
	int i;
	graph* newGraph = (graph*)malloc (sizeof (graph));
	newGraph->vertices = (struct vertex*) malloc (sizeof(struct vertex) * (size+2));
	newGraph->numberOfVertexes = size;
	newGraph->flow = 0;
	newGraph->sink = (struct vertex*)malloc(sizeof(struct vertex));
	newGraph->source = (struct vertex*)malloc(sizeof(struct vertex));
	for (i = 0; i < newGraph->numberOfVertexes + 2; i++ ) {
		newGraph->vertices[i] = createVertex (i);
	}
	newGraph->source = &newGraph->vertices[i - 1] ;
	newGraph->sink =  &newGraph->vertices[i - 2];
	newGraph->graphq = (struct queue*) malloc(sizeof(struct queue));
	newGraph->graphq->array = (int*) malloc (sizeof(int)* (size+ 2));
	return newGraph;
}


void getMinCut (graph* g) {

	int i;
	for (i = 0; i < g->numberOfVertexes +2; i++){
		if(g->vertices[i].d == INFINITY) g->vertices[i].color = 'P';

		else if (g->vertices[i].d != INFINITY)
				g->vertices[i].color = 'C';

		}
}

void doFlow (struct listNode* l, int flow) {
		l->peso -= flow;
	if (l->nextResidual != NULL)	l->nextResidual->peso += flow;
}

void getAugmentingPath(graph*g) {

	int capMin = g->sink->minCost;

	struct vertex*v = g->sink;

	while(v->path != NULL) {
			doFlow(v->path, capMin);
			v = v->predecessor;
	}
	g->flow += capMin;

}

void bfs (graph* g) {


	int i;

	for (i = 0; i < g->numberOfVertexes + 2; i++){

		g->vertices[i].d = INFINITY;
		g->vertices[i].predecessor = NULL;
		g->vertices[i].path = NULL;
		g->vertices[i].minCost = INFINITY;
		g->graphq->array[i] = INFINITY;
	}
	g->graphq->begin = 0;
	g->graphq->end = 0;


	struct queue* q = g->graphq;

	q = enqueue (q, g->source);


	link l;
	g->source->d = 0;
	vertice* v;
	while ( (v = dequeue(g, q)) != NULL ) {
			if (v->id == g->sink->id){return;}
			for (l = v->arcos_lista; l != NULL; l = l->next)
					if((l->v->d == INFINITY) && (l->peso != 0)) {
						enqueue(q, l->v);
						if(v->minCost > l->peso || v->minCost == INFINITY) l->v->minCost = l->peso;
						else l->v->minCost = v->minCost;
						l->v->d = v->d + 1;
						l->v->predecessor = v;
						l->v->path = l;
						if(l->v->id == g->sink->id) {return;}
			}
		}

	}





void  edmondsKarp (graph* g) {


	while (g->sink->d != INFINITY) {
		bfs(g);
		if(g->sink->d != INFINITY)
			getAugmentingPath(g);
	}
}

int main () {
	/*m linhas, n colunas*/
	int m, n;
	scanf("%d %d", &m, &n);

	graph* g = initGraph(n*m);
	int i, j;
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			scanf ("%d", &g->vertices[i*n+j].pesoPreto);
		}
	}

	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++){
			scanf ("%d", &g->vertices[i*n+j].pesoBranco);
		}
	}

	for (i = 0; i < m; i++) {
		for (j = 0; j < n-1; j++) {
			int weight;
			scanf("%d", &weight);
			if(weight != 0) {
			g->vertices[i*n+j].arcos_lista = add2list(g->vertices[i*n+j].arcos_lista, &g->vertices[(i*n)+j+1],weight);
			g->vertices[i*n+j+1].arcos_lista = add2list(g->vertices[i*n+j+1].arcos_lista, &g->vertices[i*n+j],weight);
			/*Cada arco tem um ponteiro para o respetivo arco na rede residual*/

			g->vertices[i*n+j].arcos_lista->nextResidual = g->vertices[i*n+j+1].arcos_lista;
			g->vertices[i*n+j+1].arcos_lista->nextResidual = g->vertices[i*n+j].arcos_lista;
		}

		}
	}

	for (i = 0; i < m-1; i++) {
		for (j = 0; j < n; j++) {
			int weight;
			scanf("%d", &weight);
			if (weight != 0) {
			g->vertices[i*n+j].arcos_lista = add2list(g->vertices[i*n+j].arcos_lista, &g->vertices[(i+1)*n+j],weight);
			g->vertices[(i+1)*n+j].arcos_lista = add2list(g->vertices[(i+1)*n+j].arcos_lista, &g->vertices[i*n+j],weight);
			g->vertices[i*n+j].arcos_lista->nextResidual = g->vertices[(i+1)*n+j].arcos_lista;
			g->vertices[(i+1)*n+j].arcos_lista->nextResidual = g->vertices[i*n+j].arcos_lista;
			}
		}
	}



	for (i = 0; i < g->numberOfVertexes; i++) {

		if(g->vertices[i].pesoPreto != 0 && g->vertices[i].pesoBranco != 0) {
			if (g->vertices[i].pesoPreto < g->vertices[i].pesoBranco) {
				g->flow += g->vertices[i].pesoPreto;
				g->vertices[i].pesoBranco -= g->vertices[i].pesoPreto;
				g->vertices[i].pesoPreto = 0;
			}
			else {
					g->flow += g->vertices[i].pesoBranco;
					g->vertices[i].pesoPreto -= g->vertices[i].pesoBranco;
					g->vertices[i].pesoBranco = 0;
			}
		}

		if (g->vertices[i].pesoPreto != 0 ) {
		g->source->arcos_lista = add2list(g->source->arcos_lista, &g->vertices[i], g->vertices[i].pesoPreto);


	}

		if (g->vertices[i].pesoBranco != 0) {
		g->vertices[i].arcos_lista = add2list(g->vertices[i].arcos_lista, g->sink, g->vertices[i].pesoBranco);

	}
}

	edmondsKarp(g);
	getMinCut(g);

	printf("%d\n\n", g->flow);
	int count = 0;
	for (i = 0; i < g->numberOfVertexes; i ++) {
		printf("%c ", g->vertices[i].color);
		count++;
		if (count == n) { printf("\n"); count = 0;}
	}

	return 0;
}
